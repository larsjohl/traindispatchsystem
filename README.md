# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Lars-Johan Larsen"  
STUDENT ID = "510246"

## Project description

A train dispatch system with a text-based menu as user-interface.


## Project structure

Apache-Maven

Sourcefiles:
src\main\java\edu\ntnu\stud\Main.java
src\main\java\edu\ntnu\stud\menubuilder\UserInterface.Java
src\main\java\edu\ntnu\stud\menubuilder\menu\Menu.Java
src\main\java\edu\ntnu\stud\menubuilder\menu\MenuItem.Java
src\main\java\edu\ntnu\stud\menubuilder\trainz\Departure.Java
src\main\java\edu\ntnu\stud\menubuilder\trainz\Station.Java

JUnit-tests:
test\java\edu\ntnu\stud\menubuilder\trainz\DepTest.Java
test\java\edu\ntnu\stud\menubuilder\trainz\StationTest.Java


## Link to repository

https://gitlab.stud.idi.ntnu.no/larsjohl/traindispatchsystem

## How to run the project

?

## How to run the tests

mvn test

## References

https://stackoverflow.com/questions/2319538/most-concise-way-to-convert-a-sett-to-a-listt
https://stackoverflow.com/questions/1235179/simple-way-to-repeat-a-string
https://stackoverflow.com/a/1814112
