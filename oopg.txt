"Kravspek"

Train dispatch system

- Departure time - hh:mm, 24h display

- Line - A stretch of rails the train traverses, multiple trains can traverse the same line at different times.

- Train Number - Unique identification number(within a 24h period).

- Destination - Self explanatory?

- Track - Whole possitive number representing a track, -1 for trains that have not been designated a track yet.

- Delay - How much a train has been dilayed in hh:mm. Set to 00:00 if the train is not delayed.


"avgrensninger"

- The train dispatch system should only handle one station.

- The train dispatch system does not handle dates, only departures within the same day.

- The time is updated manually trough the user interface.


"Funksjonelle krav"
The application is to have a text-based user interface in the form of a menu.
From this menu the operator should be able to perform (at least) the following:
 - Show/print a list of departures, sorted by current departure time.
 - Add a new departure. It shouldn't be possible to add a duplicate of a train number.
 - Assign a track to a departure, by first searching for a departure by the train number, and then add a track.
 - Add a delay to a departure, by first searching for a departure by train number, and then adding a dellay in the format hh:mm.
 - Search for departures by train number.
 - Search for departures by destination.
 - Update the current time by asking the user for a new time of day. This should not allow for setting the time of day at a earlier time of day than the current one. Train departures are to automatically be removed if the current time is set to after (departure time + delay).
 - Exit the application.

the "information board" is to show the following information in this order:
 - Departure time on the form hh:mm
 - Line
 - Train Number
 - Destination (if there is no delay this should be epty, and not even display "00:00"
 - Track (If no track is assigned, this should display nothing)



"Del 1"
Design av klasse, kodestil og dokumentasjon samt versjonskontroll og enhetstesting.
 - Understand the project:
 - Create the project as a Maven-project: OK
 - Set up version controll like GitHub or GitLab: OK
 - Implement the "entity class" representing a train departure:
 - implement a test class to test the "entity class"
 - Start writing the project report.


The documentation of the "entity class" should include
 - What the class is responsible for
 - which information the class contains, as well as which datatypes are used for each type of information with arguments for why this datatype has been chosen.
 - An assessment of which information should only be assigned at the creation of an object, and which information should be possible to mutate.
 - How the class responds to inproper or invalid data, and a description of the strategy used to achieve this.



Writing the project report:
 - Get to know the report template
 - Make the front page and write an introduction to the report.
 - Make a UseCase-diagram showing the functionality the application is going to provide to a user.
 - Start writing the chapters about theory and method.
 - Start giving a short description of which classes you are going to make for the project in the "resultat" chapter, and make a class-diagram showing dependencies between those classes.