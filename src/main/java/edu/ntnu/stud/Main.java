package edu.ntnu.stud;

import edu.ntnu.stud.menubuilder.UserInterface;

public class Main {

  public static void main(String[] args) {
    UserInterface myUserInterface = new UserInterface();
    myUserInterface.init();
    myUserInterface.start();
  }

    
}
