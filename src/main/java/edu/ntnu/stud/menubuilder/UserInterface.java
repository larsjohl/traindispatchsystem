package edu.ntnu.stud.menubuilder;

import edu.ntnu.stud.menubuilder.menu.Menu;
import edu.ntnu.stud.menubuilder.trainz.Station;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/** .
 *
 */
public class UserInterface {
  private Menu mainMenu;
  private Menu manageDeparturesMenu;
  private Menu searchDeparturesMenu;
  private Menu sortDeparturesMenu;
  private Menu editDepartureMenu;

  private String informationBoard;
  private String selectedDeparture;
  
  private List<String> selectedDepartures;
  
  private Station station;
  
  private LocalDateTime stationTime;
  
  private Duration timeTolerance;
  
  private int informationBoardCollumnWidth;
  private int informationBoardSize;
  
  private Scanner scanner;
  
  //SonarLint wants to break this.
  private Runnable mySort;
  //Do not break this.



  /** Builds a temporary menu.
   *
   * @param menuName A String representing the name of the menu.
   * @param myStation A Station representing the station the menu is related to.
   * @return a Menu representing a menu.
   */
  private Menu buildSubmenu(String menuName, Station myStation, List<String> keyList) {
    Menu tmpMenu = new Menu(menuName);
    for (String d : keyList) {
      if (myStation.getAllDepartures().contains(d)) {
        tmpMenu.newEntry(String.format("Edit %s", d), () -> {
          selectedDeparture = d;
          editDepartureMenu.displayMenu(false);
        });
      }
    }
    return tmpMenu;
  }


  /** Builds and returns a information board entry for a departure.
   *
   * @param collumnWidth An int representing the desired collumn Width of the table entry.
   * @param trainNumber A unique String representing a train or departure, serves as hashmap key.
   * @return A string representing a row on the information board table.
   */
  public String getInformationBoardEntry(int collumnWidth, String trainNumber) {
    //Note to self - Refactor this into separate class for handling Information Board ?
    StringBuilder stringBuilder = new StringBuilder("|");
    String formString = "|%-" + collumnWidth + "s|";

    //Departure Time
    stringBuilder.append(String.format(formString,
        LocalTime.of(station.getDepartureCurrentDepartureTime(trainNumber).getHour(),
            station.getDepartureCurrentDepartureTime(trainNumber).getMinute())));

    //Line
    stringBuilder.append(String.format(formString, 
        station.getDepartureLine(trainNumber)));

    //Train Number
    stringBuilder.append(String.format(formString, 
        trainNumber));

    //Destination
    stringBuilder.append(String.format(formString, 
        station.getDepartureDestination(trainNumber)));
    
    //Track
    int track = station.getDepartureTrack(trainNumber);
    if (track != -1) {
      stringBuilder.append(String.format(formString, track));
    } else {
      stringBuilder.append(String.format(formString, ""));
    }

    //Delay
    Duration delay = station.getDepartureDelay(trainNumber);
    if (!delay.isZero()) {
      LocalTime myDelay = LocalTime.of(delay.toMinutesPart(), delay.toHoursPart());
      stringBuilder.append(String.format(formString, String.format(
          "%s", myDelay)));
    } else {
      stringBuilder.append(String.format(formString, ""));
    }

    stringBuilder.append("|");

    return stringBuilder.toString();
  }

  private void updateInformationBoard() {
    List<String> informationBoardContent = station.getDeparturesByCurrentDepartureTime(
        stationTime, 
        LocalDateTime.of(stationTime.toLocalDate(), 
        LocalTime.MAX));

    informationBoardContent = station
        .sortDeparturesByCurrentDepartureTime(false, informationBoardContent);

    if (informationBoardContent.size() > informationBoardSize) {
      informationBoardContent = informationBoardContent.subList(0, informationBoardSize);
    }

    StringBuilder stringBuilder = new StringBuilder(
        String.format("INFORMATION BOARD - NEXT %d DEPARTURES - %s%n", 
            informationBoardContent.size(), 
            stationTime.toString()));

    stringBuilder.append("|");

    String formString = "|%-" + informationBoardCollumnWidth + "s|";
    
    //Departure Time
    stringBuilder.append(String.format(formString,
        "Departure Time"));
    
    //Line
    stringBuilder.append(String.format(formString,
        "Line"));

    //Train Number
    stringBuilder.append(String.format(formString,
        "Train Number"));
    
    //Destination
    stringBuilder.append(String.format(formString,
        "Destination"));
    
    //Track
    stringBuilder.append(String.format(formString,
        "Track"));
    
    //Delay
    stringBuilder.append(String.format(formString,
        "Delay"));

        
    stringBuilder.append("|");
        
    //From https://stackoverflow.com/questions/1235179/simple-way-to-repeat-a-string
    String dividerString = new String(
        new char[6]).replace("\0", "|-----------------|");
    stringBuilder.append(String.format("%n|%s|", dividerString));
          

    for (String d : informationBoardContent) {
      stringBuilder.append(String.format(
          "%n%s", getInformationBoardEntry(informationBoardCollumnWidth, d)));
    }

    informationBoard = stringBuilder.toString();

  }

  private void displayInformationBoard() {
    System.out.println(String.format("%n%s%n", informationBoard));
  }

  private void setCurrentTime() {
    System.out.printf("%nCurrent Time: %s", stationTime.toString());
    LocalDateTime newTime = LocalDateTime.of(
        stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());
    if (stationTime.isAfter(newTime)) {
      System.out.println("TimeLordException: Cannot go backwards in time.");
    } else {
      selectedDepartures = station.getDeparturesByCurrentDepartureTime(stationTime, newTime);
      for (String d : selectedDepartures) {
        station.delDeparture(d);
      }
      stationTime = newTime;
      System.out.printf("%nNew Current Time: %s", stationTime.toString());
    }
  }

  private void addDelayToUndeparted() {
    List<String> result = new ArrayList<>();
    for (String departureNumber : station
        .sortDeparturesByCurrentDepartureTime(true, station.getAllDepartures())) {
      
      if (station.getDepartureCurrentDepartureTime(departureNumber).isAfter(stationTime)) {
        result.add(departureNumber);
      }
    }

    selectedDepartures = result;
    Duration delay = inputDuration();

    if (!result.isEmpty()) {
      for (String departureNumber : result) {
        station.addDepartureDelay(delay, departureNumber, timeTolerance);
      }
    }
  }

  private Duration inputDuration() {
    int minutes = 0;
    int hours = 0;

    System.out.println("Input Hours: ");
    try {
      hours = Integer.parseInt(scanner.nextLine());
    } catch (IllegalArgumentException e) {
      System.out.println("Invalid input for hours, must be whole number.");
    }
    
    System.out.println("Input Minutes: ");
    try {
      minutes = Integer.parseInt(scanner.nextLine());
    } catch (IllegalArgumentException e) {
      System.out.println("Invalid input for minutes, must be whole number.");
    }

    Duration myNewDuration = Duration.ofHours(hours).plus(Duration.ofMinutes(minutes));

    if (minutes < 59 && hours < 23) {
      return myNewDuration;
    } else {
      System.out.printf(
          "%nCannot add a duration of %d Hours and %d.", 
          myNewDuration.toHoursPart(), myNewDuration.toMinutesPart());
      return Duration.ZERO;
    }
  }

  private void addDeparture() {
    boolean success;
    System.out.println("Enter Train Number: ");
    String trainNumber = scanner.nextLine();
    System.out.println("Enter Line: ");
    String line = scanner.nextLine();
    System.out.println("Enter Destination: ");
    String destination = scanner.nextLine();
    
    LocalDateTime departureTime = LocalDateTime.of(
        stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());

    try {
      success = station.addDeparture(
          trainNumber, line, destination, departureTime, timeTolerance);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return;
    }

    if (success) {
      System.out.println("Departure successfully created.");
    } else {
      System.out.println("Failed to create departure.");
    }
  }

  private void setTrackNumber() {
    int trackNumber = station.getDepartureTrack(selectedDeparture);
    System.out.println("Enter Track Number: ");
    try {
      trackNumber = Integer.parseInt(scanner.nextLine());
    } catch (IllegalArgumentException e) {
      System.out.println(String.format(
          "Failed to enter Track Number.%nReason:%s", e));
    }
    try {
      station.setDepartureTrack(selectedDeparture, trackNumber, timeTolerance);
    } catch (IllegalArgumentException e) {
      System.out.println(String.format(
          "Failed to set Track Number for %s to %d.%nReason:%s", 
          selectedDeparture, trackNumber, e));
    }

  }

  private void setDelayDuration() {
    try {
      station.setDepartureDelay(inputDuration(), selectedDeparture, timeTolerance);
    } catch (IllegalArgumentException e) {
      System.out.println(String.format(
          "Failed to set Delay for Departure %s.%nReason:%s", selectedDeparture, e));
    }

  }

  private void addDelayDuration() {
    try {
      station.addDepartureDelay(inputDuration(), selectedDeparture, timeTolerance);
    } catch (IllegalArgumentException e) {
      System.out.println(String.format(
          "Failed to add Delay for Departure %s.%nReason:%s", selectedDeparture, e));
    }
  }

  private void substractDelayDuration() {
    try {
      station.subDepartureDelay(inputDuration(), selectedDeparture, timeTolerance);
    } catch (IllegalArgumentException e) {
      System.out.println(String.format(
          "Failed to subtract Delay for Departure %s.%nReason:%s", selectedDeparture, e));
    }
  }


  /** Innitialize the main menu and all menu functionality.
   *
   */
  public void init() {
    stationTime = LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN);
    informationBoardCollumnWidth = 17;
    informationBoardSize = 10;
    scanner = new Scanner(System.in);
    timeTolerance = Duration.ofMinutes(1);
    station = new Station("Sesam Stasjon");

    
    //Populate Departures
    for (int i = 0; i < 23; i += 3) {
      String myTrainNumber = String.format("A-%d:0", i);
      station.addDeparture(myTrainNumber, 
          "S-A", 
          "Andeby", 
          LocalDateTime.of(stationTime.toLocalDate(), LocalTime.of(i, 0)),
          timeTolerance);
      station.setDepartureTrack(myTrainNumber, 3, timeTolerance);
    }
    
    
    String reverseSortPrompt = "Reverse Sort (y/n): ";
    mySort = (() -> {
      
      System.out.println(reverseSortPrompt);
      selectedDepartures = station.sortDeparturesByCurrentDepartureTime(
        scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
    });

    //Main Menu - Start
    mainMenu = new Menu("Main Menu");

    mainMenu.newEntry("Display Information Board", () -> {
      updateInformationBoard();
      displayInformationBoard();
    });

    mainMenu.newEntry("Manage Departures", () ->
        manageDeparturesMenu.displayMenu(true));

    mainMenu.newEntry("Set Time Of Day", () -> {
      setCurrentTime();
      updateInformationBoard();
    });

    mainMenu.newEntry("Select Default Sorting", () ->
        sortDeparturesMenu.displayMenu(true));
    //Main Menu - End

    
    //Manage Departures - Start
    manageDeparturesMenu = new Menu("Manage Departures");

    manageDeparturesMenu.newEntry("Search Departures", () ->
        searchDeparturesMenu.displayMenu(false));

    manageDeparturesMenu.newEntry("Add delay to all in queue", () -> {
      addDelayToUndeparted();
      updateInformationBoard();
    });

    manageDeparturesMenu.newEntry("Add Departure", () -> {
      addDeparture();
      updateInformationBoard();
    });

    manageDeparturesMenu.newEntry("Edit Departure by Train Number", () -> {

      System.out.println("Enter train number: ");
      String trainNumber = scanner.nextLine();
      if (station.getAllDepartures().contains(trainNumber)) {
        selectedDeparture = trainNumber;
        editDepartureMenu.displayMenu(false);
        updateInformationBoard();
      } else {
        System.out.println("Departure not found.");
      }
    });
    //Manage Departures - End


    //Find Departures By - Start
    searchDeparturesMenu = new Menu("Search Departures");

    searchDeparturesMenu.newEntry("All Departures", () -> {
      selectedDepartures = station.getAllDepartures();
      mySort.run();
      buildSubmenu("Select From All Departures", 
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });
    
    searchDeparturesMenu.newEntry("By Destination", () -> {

      System.out.println("Enter Destination: ");
      String destination = scanner.nextLine();
      selectedDepartures = station.getDeparturesByDestination(destination);
      mySort.run();
      buildSubmenu("Select From Matches by Destination",
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });

    searchDeparturesMenu.newEntry("By Original Departure Time Range", () -> {
      System.out.println("Start of Time-Range: ");
      LocalDateTime startTime = LocalDateTime.of(
          stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());

      System.out.println("End of Time-Range: ");
      LocalDateTime endTime = LocalDateTime.of(
          stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());
      
      selectedDepartures = station.getDeparturesByOriginalDepartureTime(startTime, endTime);

      mySort.run();
      buildSubmenu("Select From Matches by Original Departure Time",
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });

    searchDeparturesMenu.newEntry("By Current Departure Time Range", () -> {
      System.out.println("Start of Time-Range: ");
      LocalDateTime startTime = LocalDateTime.of(
          stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());

      System.out.println("End of Time-Range: ");
      LocalDateTime endTime = LocalDateTime.of(
          stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());

      selectedDepartures = station.getDeparturesByCurrentDepartureTime(startTime, endTime);
      mySort.run();
      buildSubmenu("Select From Matches by Current Departure Time", 
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });
    
    searchDeparturesMenu.newEntry("By Departures Before Time", () -> {

      System.out.println("Enter Time: ");
      LocalDateTime myTime = LocalDateTime.of(
          stationTime.toLocalDate(), LocalTime.MIN).plus(inputDuration());

      selectedDepartures = station.getDeparturesByCurrentDepartureTime(stationTime, myTime);
      mySort.run();
      buildSubmenu("Select From Matches by Before Time", 
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });
    
    searchDeparturesMenu.newEntry("By Delay", () -> {
      System.out.println("Minimum Delay: ");
      Duration minimumDuration = inputDuration();

      System.out.println("Maximum Delay: ");
      Duration maximumDuration = inputDuration();

      selectedDepartures = station.getDeparturesByDelay(minimumDuration, maximumDuration);
      mySort.run();
      buildSubmenu("Select From Matches by Delay", 
        station, selectedDepartures).displayMenu(true);
      updateInformationBoard();
    });
    //Find Departures By - End


    //Set Sort - Start
    sortDeparturesMenu = new Menu("Set Sort");

    sortDeparturesMenu.newEntry("Set Sort by Train Number", () -> {
      System.out.println("Set Sort by Train Number");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByTrainNumber(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });      
    });

    sortDeparturesMenu.newEntry("Set Sort by Line", () -> {
      System.out.println("Set Sort by Line");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByLine(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });

    sortDeparturesMenu.newEntry("Set Sort by Destination", () -> {
      System.out.println("Set Sort by Destination");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByDestination(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });
    
    sortDeparturesMenu.newEntry("Set Sort by Original Departure Time", () -> {
      System.out.println("Set Sort by Original Departure Time");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByOriginalDepartureTime(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });

    sortDeparturesMenu.newEntry("Set Sort by Current Departure Time", () -> {
      System.out.println("Set Sort by Current Departure Time"); 
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByCurrentDepartureTime(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });

    sortDeparturesMenu.newEntry("Set Sort by Delay", () -> {
      System.out.println("Set Sort by Delay");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByDelay(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });

    sortDeparturesMenu.newEntry("Set Sort by Track", () -> {
      System.out.println("Set Sort by Track");
      mySort = (() -> {
        
        System.out.println(reverseSortPrompt);
        selectedDepartures = station.sortDeparturesByTrack(
          scanner.nextLine().equalsIgnoreCase("Y"), selectedDepartures);
      });
    });
    //Set Sort - End


    //Edit Departure - Start
    editDepartureMenu = new Menu("Edit Departure");

    editDepartureMenu.newEntry("Set Track Number", () -> {
      setTrackNumber();
      updateInformationBoard();
    });

    editDepartureMenu.newEntry("Set Delay", () -> {
      setDelayDuration();
      updateInformationBoard();
    });

    editDepartureMenu.newEntry("Add Delay", () -> {
      addDelayDuration();
      updateInformationBoard();
    });

    editDepartureMenu.newEntry("Subtract Delay", () -> {
      substractDelayDuration();
      updateInformationBoard();
    });

  }

  /** Display the menu and start running menu functionality.
   *
   */
  public void start() {
    mainMenu.displayMenu(false);
  }
}
