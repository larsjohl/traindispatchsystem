package edu.ntnu.stud.menubuilder.menu;

/** Represent an item in a menu.
 * This class is from my solution to "Øving 11".
 *
 * @author Lars-Johan Larsen
 * @version 1.0
 * @since 0
*/
class MenuItem {
  private String label;
  private Runnable action;

  public MenuItem(String label, Runnable action) {
    this.label = label;
    this.action = action;
  }

  
  /** Gets an item's label.
   *
   * @return A String representing a label for an item menu.
   */
  public String getLabel() {
    return label;
  }

  
  /** Sets an item's label.
   *
   * @param label A String representing a menu item's label.
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /** Executes the action or set of actions specified for this menu item. 
   */
  public void executeAction() {
    if (action != null) {
      action.run();
    }
  }
  
  /** Sets the action or set of items executed by this menu item.
   *
   * @param action A Runnable representing the action of a menu item.
   */
  public void setAction(Runnable action) {
    this.action = action;
  }
}