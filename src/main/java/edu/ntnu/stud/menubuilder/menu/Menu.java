package edu.ntnu.stud.menubuilder.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/** Represents a menu.
 * This class is from my solution to "Øving 11".
 *
 * @author Lars-Johan Larsen
 * @version 1.0
 * @since 0
*/
public class Menu {
  private final String title;
  private List<MenuItem> entries;

  private static String autoPromptNextLine(Scanner scanner, String prompt) {
    System.out.printf(prompt);
    return scanner.nextLine();
  }

  private static int forceIntNextLine(Scanner scanner, String prompt, String inputError) {
    do {
      try {
        return Integer.parseInt(autoPromptNextLine(scanner, prompt));
      } catch (NumberFormatException e) {
        System.out.printf("%s%n%n", inputError);
      }
    } while (true);
  }
  
  /** Lets the user select a menu item from the menu.
   *
   * @param scanner A Scanner that takes user input.
   * @param maxChoice An int representing the number of MenuItems registered in the menu.
   * @return An int representing the user's selected choice.
   */
  private int getUserChoice(Scanner scanner, int maxChoice) {
    int choice = -1;
    do {
      choice = forceIntNextLine(scanner,
                                "Enter your choice: ", 
                                "Please enter a name from 0 to " + maxChoice);
    } while (choice < 0 || choice > maxChoice);
    return choice;
  }

  /** Makes a new Menu object.
   *
   * @param title A String representing the menu's title text.
   */
  public Menu(String title) {
    this.title = title;
    this.entries = new ArrayList<>();
  }

  /** Makes a new MenuItem and tries to add it to the Menu.
   *
   * @param label A String representing a menu item's label.
   * @param action  A Runnable representing the action of a menu item.
   */
  public void newEntry(String label, Runnable action) {
    addEntry(new MenuItem(label, action));
  }
  
  /** Adds a MenuItem to the menu.
   *
   * @param myEntry A MenuItem representing an entry in a menu.
   */
  public void addEntry(MenuItem myEntry) {
    for (MenuItem entry : this.entries) {
      if (entry.getLabel().equals(myEntry.getLabel())) {
        throw new IllegalArgumentException(
          "A MenuItem with this label already exists in this Menu");
      }
    }
    entries.add(myEntry);
  }

  /** Replaces a MenuItem in the Menu with a different MenuItem.
   *
   * @param toReplaceMenuItem  A MenuItem representing an entry in a menu.
   * @param replacemenMenuItem  A MenuItem representing an entry in a menu.
   */
  public void replaceEntry(MenuItem toReplaceMenuItem, MenuItem replacemenMenuItem) {
    this.entries.set(this.entries.indexOf(toReplaceMenuItem), replacemenMenuItem);
  }

  /** Removes a MenuItem from the Menu.
   *
   * @param myEntry A MenuItem representing an entry in a menu.
   * @return A MenuItem representing the entry that has been removed.
   */
  public MenuItem removeEntry(MenuItem myEntry) {
    MenuItem result = null;
    for (MenuItem entry : this.entries) {
      if (entry.equals(myEntry)) {
        this.entries.remove(entry);
        result = entry;
        break;
      }
    }
    return result;
  }

  /** Gets the MenuItems from this Menu in the form of a List.
   *
   * @return A List of MenuItems representing the contents of a Menu.
   */
  public List<MenuItem> getEntries() {
    return this.entries;
  }

  /** Get a MenuItem from the Menu from the specified label.
   *
   * @param label A String representing a menu item's label.
   * @return A MenuItem representing the entry that has been removed.
   */
  public MenuItem getEntryByLabel(String label) {
    MenuItem result = null;
    for (MenuItem entry : this.entries) {
      if (entry.getLabel().equals(label)) {
        result = entry;
      }
    }
    return result;
  }

  /** Get a MenuItem that has the specified position in the Menu.
   *
   * @param position An int representing the possition of a MenuItem in a Menu. 
   * @return A MenuItem representing an entry in a menu.
   */
  public MenuItem getEntryByPosition(int position) {
    MenuItem result = null;
    if (((this.entries.size()) >= position) && (position >= 1)) {
      this.entries.get(position);
    }
    return result;
  }

  /** Displays a menu object's menu entries and allows user to chose an item.
   *
   * @param exitOnChoice A boolean if a menu should exit when any choice is made.
   */
  public void displayMenu(boolean exitOnChoice) {
    int choice = 0;
    do {
      final Scanner scanner = new Scanner(System.in); 
      System.out.println("\n" + title);
      int position = 1;
      for (MenuItem entry : this.entries) {
        System.out.println((position) + ". " + entry.getLabel());
        position++;
      }
      
      System.out.println("0. Exit");

      choice = getUserChoice(scanner, entries.size());

      if (choice != 0) {
        MenuItem selectedEntry = entries.get(choice - 1);
        selectedEntry.executeAction();
        if (exitOnChoice) {
          break;
        }
      }

    } while (choice != 0);
  }
}