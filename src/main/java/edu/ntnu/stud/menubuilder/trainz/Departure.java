package edu.ntnu.stud.menubuilder.trainz;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;


/** Represents a Departure.
 * This class is responsible for keeping track og original departure time,
 * destination, delay to departure, departure track, line, and train number.
 *
 */
class Departure {
  private Duration delay;
  private int track;
  private final LocalDateTime originalDepartureTime;
  private final String destination;
  private final String line;
  private final String trainNumber; //can be replaced with a reference to a train object?
  
  

  // constructors

  /** Throws an error if invalid input is given to Departure().
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param line A String representing a train line between two locations.
   * @param destination A String representing a destination.
   * @throws IllegalArgumentException For illegal values.
   */
  private void verifyDeparture(
      String trainNumber, String line, String desination) {

    if ("0".equals(trainNumber)) {
      throw new IllegalArgumentException(
        "Train number can not be \"0\"."
      );
    }

    if (line.isBlank() || line.isEmpty()) {
      throw new IllegalArgumentException(
        "Departure must have a line"
      );
    }

    if (desination.isBlank() || desination.isEmpty()) {
      throw new IllegalArgumentException(
        "Departure must have a destination."
      );
    }
  }


  /** Constructs a Departure object, representing a departure.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param line A String representing a train line between two locations.
   * @param destination A String representing the destination of this departure.
   * @param departureTime A LocalDateTime object representing original time of Departure. 
   *      The LocalDateTime datatype was chosen as the suggested datatype "LocalTime" 
   *      appears to handle any underflow or overflow of duration by looping the value,
   *      and thus necessitating unnecessary complexity in the handling of such eventualities. 
   */
  public Departure(String trainNumber, String line, 
      String destination, LocalDateTime departureTime) {
    verifyDeparture(trainNumber, line, destination);
    this.trainNumber = trainNumber;
    this.line = line;
    this.destination = destination;
    this.originalDepartureTime = departureTime;

    //set defaults, must be changed later
    this.track = -1;
    this.delay = Duration.ZERO;
  }

  // delay methods

  /** Throws an error if invalid input is given to setDelay().
   *
   * @param duration A Duration representing a delay.
   * @throws IllegalArgumentException for illegal durations.
   */
  private void verifySetDelay(Duration duration) {
    if (this.originalDepartureTime.plus(duration).isBefore(this.originalDepartureTime)) {
      throw new IllegalArgumentException(
        "The new delay duration makes the departure before originalDeparture.");
    }
  }

  /** Sets a delay from this departure's departureTime(LocalDateTime).
   *
   * @param duration A Duration to add to the LocalDateTime of the departure.
   * @throws IllegalArgumentException - if the Duration would result in an invalid departure time.
   */
  public void setDelay(Duration duration) {
    verifySetDelay(duration);
    this.delay = duration;
  }

  /** Gets the Duration representing the delay for this departure.
   *
   * @return a Duration representing a delay.
   */
  public Duration getDelay() {
    return this.delay;
  }


  // track methods

  /** Throws an error if invalid input is given to setTrack().
   *
   * @param trackNumber An int representing a track.
   * @throws IllegalArgumentException if the provided track number is less than 0 and not -1.
   */
  private void verifySetTrack(int trackNumber) {
    if ((trackNumber <= 0) && (trackNumber != -1)) {
      throw new IllegalArgumentException("""
        Cannot assign non-existant track to a departure. 
        Please assign a track number higher than 0,
        or -1 to set this departure to not have a departure assigned.""");
    }
  }

  /** Sets an int representing the track number for this departure.
   *
   * @param trackNumber A possitive int above 0 representing a track, or -1 for no track set.
   * @throws IllegalArgumentException if the provided track number is less than 0 and not -1.
   */
  public void setTrack(int trackNumber) {
    verifySetTrack(trackNumber);
    this.track = trackNumber;
  }

  /** Gets an int representing the current track number for this departure.
   *
   * @return A possitive int above 0 representing a track, or -1 if no track is set.
   */
  public int getTrack() {
    return this.track;
  }


  // departure methods


  /** Gets the LocalDateTime representing the current departure time, 
   * factoring in the current delay.
   *
   * @return a LocalDateTime representing a time of the day.
   */
  public LocalDateTime getCurrentDepartureTime() {
    return this.originalDepartureTime.plus(this.delay);
  }

  /** Gets LocalDateTime representing the original departure time, 
   * without factoring in the current delay.
   *
   * @return a LocalDateTime representing a time of the day.
   */
  public LocalDateTime getOriginalDepartureTime() {
    return this.originalDepartureTime;
  }


  // destination methods

  /** Gets a String representing the destination of this departure.
   *
   * @return A String representing a destination.
   */
  public String getDestination() {
    return this.destination;
  }

  
  // line methods
  
  /** Gets the line for this departure.
   *
   * @return A String representing a line.
   */
  public String getLine() {
    return this.line;
  }


  // trainNumber methods

  /** Gets the train number for this departure.
   *
   * @return A unique String representing a train or departure.
   */
  public String getTrainNumber() {
    return this.trainNumber;
  }


  /* Comparators inspired by 
   * https://stackoverflow.com/a/1814112
   */

  public static final Comparator<Departure> compareByTrainNumber = (self, other) ->
      self.trainNumber.compareTo(other.trainNumber);

  public static final Comparator<Departure> compareByLine = (self, other) ->
      self.line.compareTo(other.line);

  public static final Comparator<Departure> compareByDestination = (self, other) ->
      self.destination.compareTo(other.destination);

  public static final Comparator<Departure> compareByOriginalTime = (self, other) ->
      self.originalDepartureTime.compareTo(other.originalDepartureTime);

  public static final Comparator<Departure> compareByCurrentTime = (self, other) -> 
      self.getCurrentDepartureTime().compareTo(other.getCurrentDepartureTime());

  public static final Comparator<Departure> compareByDelay = (self, other) ->
      self.delay.compareTo(other.delay);

  public static final Comparator<Departure> compareByTrack = (self, other) ->
      Integer.compare(self.track, other.track);

}