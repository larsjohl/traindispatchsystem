
package edu.ntnu.stud.menubuilder.trainz;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/** Represents a train station.
 * Responsible for storing and managing Departures.
 * Uses a HashMap for storing Departures.
 */
public class Station {
  private Map<String, Departure> departures;
  private final String stationName;
  

  /** Validates that two departures won't be on the same track at the same time.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param track An int representing a track number.
   * @param timeTolerance a Duration in which a line or track is occupied by a departure.
   */
  private void checkTrackTimeConflicts(Departure departure, Duration timeTolerance) {
    if (this.departures.isEmpty() || departure.getTrack() == -1) {
      return;
    }

    int myCurrentTrack = departure.getTrack();
    List<String> possibleTrackConflicts = getDeparturesByTrack(myCurrentTrack);
    List<String> possibleTimeConflicts = getDeparturesByCurrentDepartureTime(
          departure.getCurrentDepartureTime().minus(timeTolerance), 
          departure.getCurrentDepartureTime().plus(timeTolerance));

    if (!(possibleTimeConflicts.isEmpty() && possibleTrackConflicts.isEmpty())) {
      for (String c : possibleTrackConflicts) {
        if (possibleTimeConflicts.contains(c)) {
          Departure conflictDeparture = this.departures.get(c);
          throw new IllegalArgumentException(String.format(
                "The track %s is occupied by %s at %s",
                myCurrentTrack,
                c,
                conflictDeparture.getCurrentDepartureTime().toString()));
        }
      }
    }
  }

  private void checkTrackTimeConflicts(
      Duration myNewDelay, Departure departure, Duration timeTolerance) {
    if (this.departures.isEmpty() || departure.getTrack() == -1) {
      return;
    }

    int myTrack = departure.getTrack();
    LocalDateTime myNewDeparture = departure.getOriginalDepartureTime().plus(myNewDelay);
    List<String> possibleTrackConflicts = getDeparturesByTrack(myTrack);
    List<String> possibleTimeConflicts = getDeparturesByCurrentDepartureTime(
          myNewDeparture.minus(timeTolerance), 
          myNewDeparture.plus(timeTolerance));

    if (!(possibleTimeConflicts.isEmpty() && possibleTrackConflicts.isEmpty())) {
      for (String c : possibleTrackConflicts) {
        if (possibleTimeConflicts.contains(c)) {
          Departure conflictDeparture = this.departures.get(c);
          throw new IllegalArgumentException(String.format(
                "The track %s is occupied by %s at %s",
                myTrack,
                c,
                conflictDeparture.getCurrentDepartureTime().toString()));
        }
      }
    }
  }

  /** Validates that two departures on the same line aren't too close.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param line A string representing a line between origin and destination.
   * @param timeTolerance a Duration in which a line or track is occupied by a departure.
   */
  private void checkLineTimeConflicts(Departure departure, Duration timeTolerance) {
    if (this.departures.isEmpty()) {
      return;
    }
    String myLine = departure.getLine();
    List<String> possibleLineConflicts = getDeparturesByLine(myLine);
    List<String> possibleTimeConflicts = getDeparturesByCurrentDepartureTime(
          departure.getCurrentDepartureTime().minus(timeTolerance), 
          departure.getCurrentDepartureTime().plus(timeTolerance));

    if (!(possibleTimeConflicts.isEmpty() && possibleLineConflicts.isEmpty())) {
      for (String c : possibleLineConflicts) {
        if (possibleTimeConflicts.contains(c)) {
          Departure conflictDeparture = this.departures.get(c);
          if (!c.equals(departure.getTrainNumber())) {
            throw new IllegalArgumentException(String.format(
                  "The line %s is occupied by %s departing at %s",
                  myLine,
                  c,
                  conflictDeparture.getCurrentDepartureTime().toString()));
          }
        }
      }
    }
  }

  private void checkLineTimeConflicts(
      Duration myNewDelay, Departure departure, Duration timeTolerance) {
    if (this.departures.isEmpty()) {
      return;
    }
    String myLine = departure.getLine();
    LocalDateTime myNewDeparture = departure.getOriginalDepartureTime().plus(myNewDelay);
    List<String> possibleLineConflicts = getDeparturesByLine(myLine);
    List<String> possibleTimeConflicts = getDeparturesByCurrentDepartureTime(
          myNewDeparture.minus(timeTolerance), 
          myNewDeparture.plus(timeTolerance));

    if (!(possibleTimeConflicts.isEmpty() && possibleLineConflicts.isEmpty())) {
      for (String c : possibleLineConflicts) {
        if (possibleTimeConflicts.contains(c)) {
          Departure conflictDeparture = this.departures.get(c);
          if (!c.equals(departure.getTrainNumber())) {
            throw new IllegalArgumentException(String.format(
                  "The line %s is occupied by %s departing at %s",
                  myLine,
                  c,
                  conflictDeparture.getCurrentDepartureTime().toString()));
          }
        }
      }
    }
  }

  
  /** Constructs a station object to hold and manage departures.
   *
   * @param stationName A String represeenting this station.
   */
  public Station(String stationName) {
    this.stationName = stationName;
    this.departures = new HashMap<>();
  }
  
  public String getStationName() {
    return this.stationName;
  }

  /** Get a list of train numbers for all departures.
   *
   * @return A List<> containing Strings that represent the train numbers for all departures.
   */
  public List<String> getAllDepartures() {
    // Source: https://stackoverflow.com/questions/2319538/most-concise-way-to-convert-a-sett-to-a-listt
    return new ArrayList<>(departures.keySet());
  }


  /** Adds a new departure to the Station.
   *
   * @param trainNumber A unique String representing a train or departure, serves as hashmap key.
   * @param line A String representing a train line between two locations.
   * @param destination A String representing a destination.
   * @param departureTime  A LocalDateTime object representing original time of Departure. 
   * @return boolean, true if the departure was successfully added, false otherwise.
   */
  public boolean addDeparture(String trainNumber, String line, 
      String destination, LocalDateTime departureTime, Duration timeTolerance) {
    if (!departures.containsKey(trainNumber)) {
      Departure myNewDeparture = new Departure(trainNumber, line, destination, departureTime);
      checkLineTimeConflicts(myNewDeparture, timeTolerance);
      departures.put(trainNumber, myNewDeparture);
      return true;
    } else {
      return false;
    }
  }


  /** Removes a departure from the station.
   *
   * @param trainNumber  A unique String representing a train or departure, serves as hashmap key.
   * @return boolean, true if the departure was successfully deleted, false otherwise.
   */
  public boolean delDeparture(String trainNumber) {
    if (!departures.containsKey(trainNumber)) {
      return false;
    } else {
      departures.remove(trainNumber);
      return true;
    }
  }


  /** Sorts a List<> of Strings representing train numbers by train numbers.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByTrainNumber(boolean reverse, List<String> trainNumbers) {
    Collections.sort(trainNumbers);
    if (reverse) {
      Collections.reverse(trainNumbers);
    }
    return trainNumbers;
  }


  /** Gets a list of train numbers with matching line.
   *
   * @param line A String representing a train line between two locations.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByLine(String line) {
    List<String> result = new ArrayList<>();
    for (Departure d : departures.values()) {
      if (d.getLine().equals(line)) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by Line.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByLine(boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByLine);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Gets a list of train numbers with matching line.
   *
   * @param destination A String representing a destination.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByDestination(String destination) {
    List<String> result = new ArrayList<>();
    for (Departure d : departures.values()) {
      if (d.getDestination().equals(destination)) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by Destination.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByDestination(boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByDestination);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Gets a list of train numbers with original departure times between specified times.
   *
   * @param rangeStart a LocalDateTime representing the start of a time range.
   * @param rangeEnd a LocalDateTime representing the end of a time range.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByOriginalDepartureTime(
      LocalDateTime rangeStart, LocalDateTime rangeEnd) {
    List<String> result = new ArrayList<>();
    for (Departure d : this.departures.values()) {
      LocalDateTime myTime = d.getOriginalDepartureTime();
      if ((myTime.isAfter(rangeStart) && myTime.isBefore(rangeEnd)) 
          || myTime.equals(rangeStart) 
          || myTime.isEqual(rangeEnd)) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by originalDepartureTime.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByOriginalDepartureTime(
      boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByOriginalTime);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Gets a list of train numbers with current departure times between specified times.
   *
   * @param rangeStart a LocalDateTime representing the start of a time range.
   * @param rangeEnd a LocalDateTime representing the end of a time range.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByCurrentDepartureTime(
      LocalDateTime rangeStart, LocalDateTime rangeEnd) {
    List<String> result = new ArrayList<>();
    for (Departure d : departures.values()) {
      LocalDateTime myTime = d.getCurrentDepartureTime();
      if ((myTime.isAfter(rangeStart) && myTime.isBefore(rangeEnd)) 
          || myTime.equals(rangeStart) 
          || myTime.isEqual(rangeEnd)) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by currentDepartureTime.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByCurrentDepartureTime(
      boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByCurrentTime);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Inclusively gets a list of train numbers with delays from min to max delay.
   *
   * @param delayMin a Duration representing the minimum delay to match.
   * @param delayMax a Duration representing the maximum delay to match.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByDelay(Duration delayMin, Duration delayMax) {
    List<String> result = new ArrayList<>();
    for (Departure d : departures.values()) {
      Duration myTime = d.getDelay();
      if (((myTime.compareTo(delayMin) >= 0) && (myTime.compareTo(delayMax) <= 0)) 
          || myTime.equals(delayMin) 
          || myTime.equals(delayMax)) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by delay.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByDelay(
      boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByDelay);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Gets a list of train numbers with matching track.
   *
   * @param track A String representing a track. Must be whole number above 0, or -1 for no track.
   * @return A List<> of Strings representing train numbers.
   */
  public List<String> getDeparturesByTrack(int track) {
    List<String> result = new ArrayList<>();
    for (Departure d : departures.values()) {
      if (d.getTrack() == track) {
        result.add(d.getTrainNumber());
      }
    }
    return result;
  }


  /** Sorts a List<> of Strings representing train numbers by Track.
   *
   * @param reverse A boolean representing if the list should be returned in reverse order.
   * @param trainNumbers A List<> containing trainNumber Strings.
   * @return A List<> containing trainNumber Strings.
   */
  public List<String> sortDeparturesByTrack(
      boolean reverse, List<String> trainNumbers) {
    List<Departure> workingList = new ArrayList<>();
    List<String> result = new ArrayList<>();
    for (String n : trainNumbers) {
      workingList.add(this.departures.get(n));
    }
    Collections.sort(workingList, Departure.compareByTrack);
    if (reverse) {
      Collections.reverse(workingList);
    }
    for (Departure d : workingList) {
      result.add(d.getTrainNumber());
    }
    return result;
  }


  /** Gets the track number for a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A possitive int larger than 0 representing a track number. -1 if no track is set.
   */
  public int getDepartureTrack(String trainNumber) {
    return this.departures.get(trainNumber).getTrack();
  }


  /** Sets the track number for a Departure object with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param track An int representing a track number.
   * @return a boolean, false in the event of an illegal argument exception, otherwise true.
   */
  public boolean setDepartureTrack(String trainNumber, int track, Duration timeTolerance) {
    Departure myDeparture = this.departures.get(trainNumber);
    Departure newDeparture;

    try {
      newDeparture = new Departure(trainNumber, myDeparture.getLine(), 
        myDeparture.getDestination(), 
        myDeparture.getOriginalDepartureTime());
      newDeparture.setTrack(track);
    } catch (IllegalArgumentException e) {
      return false;
    }

    checkTrackTimeConflicts(newDeparture, timeTolerance);

    try {
      myDeparture.setTrack(track);
      return true;
    } catch (IllegalArgumentException e) {
      System.out.println("");
      return false;
    }
  }


  /** Get the original departure time for a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A LocalDateTime representing the original departure time for a departure.
   */
  public LocalDateTime getDepartureOriginalDepartureTime(String trainNumber) {
    return this.departures.get(trainNumber).getOriginalDepartureTime();
  }


  /** Get the current departure time for a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A LocalDateTime representing the current departure time for a departure.
   */
  public LocalDateTime getDepartureCurrentDepartureTime(String trainNumber) {
    return this.departures.get(trainNumber).getCurrentDepartureTime();
  }


  /** Get the current delay for a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A Duration representing the delay for a departure.
   */
  public Duration getDepartureDelay(String trainNumber) {
    return this.departures.get(trainNumber).getDelay();
  }


  /** Sets a new delay for a departure.
   *
   * @param delay A Duration representing the current deviation from original departure time.
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @param timeTolerance a Duration in which a line or track is occupied by a departure.
   * @return a boolean, false if an illegal argument exception occured, otherwise true.
   */
  public Boolean setDepartureDelay(Duration delay, String trainNumber, Duration timeTolerance) {
    Departure myDeparture = this.departures.get(trainNumber); 
    checkTrackTimeConflicts(delay, myDeparture, timeTolerance);
    checkLineTimeConflicts(delay, myDeparture, timeTolerance);
    try {
      this.departures.get(trainNumber).setDelay(delay);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }


  /** Adds additional delay to the current delay for a departure with specified train number.
   *
   * @param delay  A Duration representing the additional deviation from original departure time.
   * @param trainNumber A String representing the unique identifier of a train or departure.
   */
  public void addDepartureDelay(Duration delay, String trainNumber, Duration timeTolerance) {
    setDepartureDelay(this.departures.get(trainNumber).getDelay().plus(delay),
        trainNumber, timeTolerance);
  }


  /** Subtracts delay from the current delay for a departure with specified train number.
   *
   * @param delay  A Duration representing the additional deviation from original departure time.
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A boolean, false if the resulting delay would make the current departure too early. 
   */
  public boolean subDepartureDelay(Duration delay, String trainNumber, Duration timeTolerance) {
    Departure myDeparture = this.departures.get(trainNumber);
    Duration newDelay = myDeparture.getDelay().minus(delay);
    if (myDeparture
        .getCurrentDepartureTime()
        .minus(newDelay)
        .isBefore(myDeparture
        .getOriginalDepartureTime())) {
      setDepartureDelay(newDelay, trainNumber, timeTolerance);
      return true;
    } else {
      return false;
    }
  }


  /** Gets the destination of a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A String representing a destination.
   */
  public String getDepartureDestination(String trainNumber) {
    return this.departures.get(trainNumber).getDestination();
  }


  /** Gets the line for a Departure with specified train number.
   *
   * @param trainNumber A String representing the unique identifier of a train or departure.
   * @return A string representing a train line.
   */
  public String getDepartureLine(String trainNumber) {
    return this.departures.get(trainNumber).getLine();
  }
}