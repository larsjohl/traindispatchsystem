package edu.ntnu.stud.menubuilder.trainz;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Duration;
import java.time.LocalDateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/** Tests the Departure class.
 * Responsible for testing that the Departure class can correctly function.
 * for storing and manipulating details relating to a train departure.
 *
 */
class DepTest {

  private Departure departure;

  /** Tests the behaviour of the Departure constructor.
   *
   */
  @Nested
  @DisplayName("Tests Departure Creation")
  class DepartureCreationTests {

    /** Tests that departureCreation correctly
     * creates departures from valid values, and
     * throws the correct errors for invalid values.
     *
     * @param trainNumber A String representing the unique identifier of a train or departure. 
     * @param line A string representing a line between origin and destination.
     * @param destination A String representing a destination.
     * @param trackNumber A String representing a track. 
     *      Must be whole number above 0, or -1 for no track.
     * @param possitive A boolean controlling of the test data is to
     *      be run trough a possitive or negative test.
     */
    @ParameterizedTest
    @CsvSource({
      "1, AB123, TestDestination, -1,true",
      "0, AB123, TestDestination, 0, false",
      "0, AB123, TestDestination, -1, false",
      "1, '', '', -1, false",
      "1, AB123, '', -1, false",
      "1, '', 'TestDestination', -1, false"
    })
    @DisplayName("Departure Creation Possitive and Negative ParamaterizedTest")
    void testDepartureCreation(
        String trainNumber, String line, String destination, int trackNumber, boolean possitive) {

      //Arrange
      LocalDateTime departureTime = LocalDateTime.of(0, 1, 1, 12, 0);
      Duration duration = Duration.ZERO;
      
      //Act
      try { 
        Departure departure = new Departure(trainNumber, line, destination, departureTime);
        if (!possitive) {
          fail("Negative Test failed: Object creation was successfull.");
        }
        //Assert
        try {
          assertEquals(trainNumber, departure.getTrainNumber());
          assertEquals(line, departure.getLine());
          assertEquals(destination, departure.getDestination());
          assertEquals(departureTime, departure.getOriginalDepartureTime());
          assertEquals(trackNumber, departure.getTrack());
          assertEquals(duration, departure.getDelay());
        } catch (AssertionError r) {
          if (possitive) {
            fail("Test failed: " + r.getMessage());
          }
        }
      } catch (IllegalArgumentException e) {
        if (possitive) {
          fail("Test failed: " + e.getMessage());
        } else {
          assertInstanceOf(IllegalArgumentException.class, e);
        }
      }

    }
    
    @Nested
    @DisplayName("Tests Delay")
    class DelayTests {

      /** Creates default values for delay tests.
       *
       */
      @BeforeEach
      void seUp() {
        // Arrange
        String trainNumber = "123";
        String line = "LineA";
        String destination = "DestinationA";
        LocalDateTime departureTime = LocalDateTime.of(0, 1, 1, 12, 0);
        departure = new Departure(trainNumber, line, destination, departureTime);
      }

      /** Removes leftover values from each test
       * to ensure test consistency.
       *
       */
      @AfterEach
      void tearDown() {
        departure = null;
      }

      /** Tests that setDelay correctly sets a valid delay.
       *
       */
      @Test
      @DisplayName("Set Delay Test")
      void testSetDelay() {
        // Arrange
        Duration delay = Duration.ofMinutes(10);

        // Act
        departure.setDelay(delay);

        // Assert
        try {
          assertEquals(delay, departure.getDelay());
        } catch (AssertionError e) {
          fail("Test failed: " + e.getMessage());
        }
      }

      /** tests that setDelay throws the correct exception
       * when the delay is negative.
       *
       */
      @Test
      @DisplayName("Set Delay Underflow Duration")
      void testSetDelayUnderflowDuration() {
        // Arrange
        Duration delay = Duration.ofMinutes(-10);

        // Act - Assert
        assertThrows(IllegalArgumentException.class, () -> departure.setDelay(delay));
      }
    }
  }
}