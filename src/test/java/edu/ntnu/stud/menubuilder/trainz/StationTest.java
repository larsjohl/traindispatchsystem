package edu.ntnu.stud.menubuilder.trainz;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


/** Testclass for the Station class.
 * Responsible for testing that the Station class can correctly function
 * as a registry for Departure objects.
 *
 */
class StationTest {

  private Station station;
  private String trainNumber = "1";
  private String line = "AB123";
  private String destination = "Testination";
  private LocalDateTime departureTime = LocalDateTime.now();
  private Duration timeTolerance = Duration.ofMinutes(5);

  @Nested
  @DisplayName("Test creation of Station object")
  class StationCreationTests {

    /** Tests the creation of a new station object.
     *
     */
    @Test
    @DisplayName("Valid Station creation Test")
    void testValidStationCreation() {

      //Arrange
      String stationName = "TestStation";

      //Act
      station = new Station(stationName);

      //Assert
      assertNotNull(station);
      assertEquals(stationName, station.getStationName());
    }
  }

  /** Tests methods related to storing and managing departure objects.
   *
   */
  @Nested
  @DisplayName("Tests Departure Management")
  class DepartureManagementTests {

    /** Sets prepares the departure registry and
     * sets default values to be used in tests. 
     *
     */
    @BeforeEach
    void setUp() {
      //Arrange
      station = new Station("BeforeEach Station");
      trainNumber = "1";
      line = "AB123";
      destination = "Testination";
      departureTime = LocalDateTime.now();
      timeTolerance = Duration.ofMinutes(5);
    }

    /** Removes leftovers from tests to ensure test consistency.
     *
     */
    @AfterEach
    void tearDown() {
      station = null;
    }

    /** Tests that addDeparture correctly
     * adds new departures to the departure registry.
     *
     */
    @Test
    @DisplayName("Add Departure Test")
    void testAddDeparturePossitive() {

      // Act
      boolean added = station.addDeparture(
          trainNumber, line, destination, departureTime, timeTolerance);

      // Assert
      assertTrue(added);
      assertEquals(List.of(trainNumber), station.getAllDepartures());
    }

    /** Tests that addDeparture correctly reports
     * failure to create departure 
     * when there is an existing departure
     * with the same train number.
     *
     */
    @Test
    @DisplayName("Add Departure Train Number Conflict Test")
    void testAddDepartureTrainNumberConflict() {
      // Arrange
      //String trainNumber = "1";
      String altLine = "456CD";
      String altDestination = "Alternate Testination";
      //LocalDateTime altDepartureTime = LocalDateTime.now();
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act
      boolean addedConflict = station.addDeparture(
          trainNumber, altLine, altDestination, departureTime, timeTolerance);

      // Assert
      assertFalse(addedConflict);
    }

    /** Tests that addDeparture throws the correct exception
     * when thereare scheduling conflicts with
     * existing departures on the same line.
     *
     */
    @Test
    @DisplayName("Add Departure Minus-timeTolerance Line Conflict Negative Test")
    void testAddDepartureLineTimeRangeMinusPosConflict() {
      // Arrange
      String altTrainNumber = "2";
      //String altLine = "456CD";
      String altDestination = "Alternate Testination";
      LocalDateTime altDepartureTime = LocalDateTime.now().plusMinutes(2);
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act, Assert
      assertThrows(IllegalArgumentException.class, () -> 
          station.addDeparture(
            altTrainNumber, line, altDestination, altDepartureTime, timeTolerance));
    }

    /** Tests that addDeparture throws the correct exeption
     * when there are scheduling conflicts with 
     * existing departures on the same line.
     *
     */
    @Test
    @DisplayName("Add Departure Pluss-timeTolerance Line Conflict Negative Test")
    void testAddDepartureLineTimeRangePlusPosConflict() {
      // Arrange
      String altTrainNumber = "2";
      //String altLine = "456CD";
      String altDestination = "Alternate Testination";
      LocalDateTime altDepartureTime = LocalDateTime.now().plusMinutes(2);
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act, Assert
      assertThrows(IllegalArgumentException.class, () -> 
          station.addDeparture(
            altTrainNumber, line, altDestination, altDepartureTime, timeTolerance));
    }
    
    /** Tests that addDeparture correctly reports
     * adds departures when there are no
     * scheduling conflicts with existing departures on the same line.
     *
     */
    @Test
    @DisplayName("Add Departure Minus-timeTolerance Line Conflict Possitive Test")
    void testAddDepartureLineTimeRangeMinusNegConflict() {
      // Arrange
      String altTrainNumber = "2";
      //String altLine = "456CD";
      String altDestination = "Alternate Testination";
      LocalDateTime altDepartureTime = LocalDateTime.now().minusMinutes(6);
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act
      boolean addedConflict = station.addDeparture(
          altTrainNumber, line, altDestination, altDepartureTime, timeTolerance);

      // Assert
      assertTrue(addedConflict);
    }

    /** Tests that addDeparture correctly
     * adds departures when there are no
     * scheduling conflicts with existing departures on the same line.
     *
     */
    @Test
    @DisplayName("Add Departure Plus-timeTolerance Conflict Possitive Test")
    void testAddDepartureLineTimeRangePlusNegConflict() {
      // Arrange
      String altTrainNumber = "2";
      //String altLine = "456CD";
      String altDestination = "Alternate Testination";
      LocalDateTime altDepartureTime = LocalDateTime.now().plusMinutes(6);
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act
      boolean addedConflict = station.addDeparture(
          altTrainNumber, line, altDestination, altDepartureTime, timeTolerance);

      // Assert
      assertTrue(addedConflict);
    }

    /** Tests that getAllDepartures correctly
     * returns an empty list if 
     * there are no departures in the departure registry.
     *
     */
    @Test
    @DisplayName("getAllDepartures Negative Test")
    void testGetAllDeparturesNegative() {

      // Act
      List<String> noDepartures = station.getAllDepartures();

      // Assert
      assertTrue(noDepartures.isEmpty());
    }

    /** Tests that getAllDepartures correctly
     * returns a list containing train numbers representing departures.
     *
     */
    @Test
    @DisplayName("getAllDepartures populated Test")
    void testGetAllDeparturesPopulated() {
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act
      List<String> notNoDepartures = station.getAllDepartures();

      // Assert
      assertTrue(notNoDepartures.contains(trainNumber));
    }

    /** Tests that delDeparture correctly 
     * reports not being able to delete the specified departure
     * from the departure registry.
     *
     */
    @Test
    @DisplayName("delDeparture Negative Test")
    void testDelDepartureNeg() {

      // Arrange
      String delTrainNumber = "DelTest";
      
      // Act
      boolean hasDeleted = station.delDeparture(delTrainNumber);

      // Assert
      assertFalse(hasDeleted);
    }

    /** Tests that delDeparture correctly 
     * deletes departures from the departure registry.
     *
     */
    @Test
    @DisplayName("delDeparture Possitive Test")
    void testDelDeparturePos() {

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      
      // Act
      boolean hasDeleted = station.delDeparture(trainNumber);

      // Assert
      assertTrue(hasDeleted);
      assertTrue(station.getAllDepartures().isEmpty());
    }

    /** Tests that sortDeparturesBytrainNumber correctly
     * sorts departures alphabetically by train number.
     *
     */
    @Test
    @DisplayName("sortDeparturesByTrainNumber Test")
    void testSortDeparturesByTrainNumber() {
      // Arrange
      List<String> trainNumbers = Arrays.asList("3", "A", "2", "1", "7", "Fish", "10");
      List<String> expectedSortedList = Arrays.asList("1", "10", "2", "3", "7", "A", "Fish");

      // Act
      List<String> sortedList = station.sortDeparturesByTrainNumber(false, trainNumbers);

      // Assert
      assertEquals(expectedSortedList, sortedList);
    }

    /** Tests that sortDeparturesByTrainNumber correctly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByTrainNumber Reverse Test")
    void testSortDeparturesByTrainNumberRev() {
      // Arrange
      List<String> trainNumbers = Arrays.asList("3", "A", "2", "1", "7", "Fish", "10");
      List<String> expectedSortedList = Arrays.asList("Fish", "A", "7", "3", "2", "10", "1");

      // Act
      List<String> sortedList = station.sortDeparturesByTrainNumber(true, trainNumbers);

      // Assert
      assertEquals(expectedSortedList, sortedList);
    }

    /** Tests that getDeparturesByLine correctly
     * returns an empty list when there are no matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByLine Negative Test")
    void testGetByDeparturesLineNegative() {

      // Act
      List<String> noDepartures = station.getDeparturesByLine("Line-A");

      // Assert
      assertTrue(noDepartures.isEmpty());
    }

    /** Tests that getDeparturesByLine correctly
     * compiles a list of only matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByLine Test") 
    void testGetDeparturesByLinePopulated() {

      // Arrange
      station.addDeparture("A", "Line-A",
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("B", "Line-A", 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("C", "Line-A", 
          destination, departureTime.plusMinutes(18), timeTolerance);
      station.addDeparture("D", "Line-B", 
          destination, departureTime.plusMinutes(24), timeTolerance);
      station.addDeparture("E", "Line-B", 
          destination, departureTime.plusMinutes(30), timeTolerance);
      station.addDeparture("F", "Line-B", 
          destination, departureTime.plusMinutes(36), timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> departuresByLine = station.getDeparturesByLine("Line-A");

      // Assert
      boolean haspassed = departuresByLine.containsAll(expectedList)
          && !departuresByLine.contains("D")
          && !departuresByLine.contains("E")
          && !departuresByLine.contains("F");
      assertTrue(haspassed);
    }

    /** Tests that sortDeparturesByLine correctly
     * returns an empty list when there are no matching departures.
     *
     */
    @Test
    @DisplayName("sortDeparturesByLine Negative Test") 
    void testSortDeparturesByLineNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByLine(
            false, failTrainNumbers);
      });
    }

    /** Tests that sortDeparturesByLine correctly
     * sorts departures alphabetically by Line.
     *
     */
    @Test
    @DisplayName("sortDeparturesByLine Test") 
    void testSortDeparturesByLine() {

      // Arrange
      station.addDeparture("B", "Line-B", 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", "Line-C", 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", "Line-A", 
          destination, departureTime.plusMinutes(18), timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByLine(false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that sortDeparturesByLine correctly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByLine Reverse Test") 
    void testSortDeparturesByLineRev() {

      // Arrange
      station.addDeparture("B", "Line-B", 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", "Line-C", 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", "Line-A", 
          destination, departureTime.plusMinutes(18), timeTolerance);
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByLine(true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that getDeparturesByDestination correctly
     * returns an empty list when there are no matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByDestination  Negative Test")
    void testGetDeparturesByDestinationNeg() {
     
      // Act
      List<String> noDepartures = station.getDeparturesByDestination("Invalid Testination");

      // Assert
      assertTrue(noDepartures.isEmpty()); 
    }

    /** Tests that getDeparturesByDestination correctly
     * compiles a list of only matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByDestination Possitive Test") 
    void testGetDeparturesByDestinationPopulated() {

      // Arrange
      station.addDeparture("A", line,
          "Testination-A", departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("B", line, 
          "Testination-A", departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("C", line, 
          "Testination-A", departureTime.plusMinutes(18), timeTolerance);
      station.addDeparture("D", line, 
          "Testination-B", departureTime.plusMinutes(24), timeTolerance);
      station.addDeparture("E", line, 
          "Testination-B", departureTime.plusMinutes(30), timeTolerance);
      station.addDeparture("F", line, 
          "Testination-B", departureTime.plusMinutes(36), timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> departuresByDestination = station.getDeparturesByDestination("Testination-A");

      // Assert
      boolean haspassed = departuresByDestination.containsAll(expectedList)
          && !departuresByDestination.contains("D")
          && !departuresByDestination.contains("E")
          && !departuresByDestination.contains("F");
      assertTrue(haspassed);
    }

    /** Tests that sortDeparturesByDestination
     * throws the correct exception when
     * trying to sort departures that do not exist.
     *
     */
    @Test
    @DisplayName("sortDeparturesByDestination Negative Test")
    void testSortDeparturesByDestinationNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByDestination(
            false, failTrainNumbers);
      });
    }

    /** Tests that sortDeparturesByDestination correctly
     * sorts departures by destination.
     *
     */
    @Test
    @DisplayName("sortDeparturesByDestination Test") 
    void testSortDeparturesByDestination() {

      // Arrange
      station.addDeparture("B", line, 
          "Testination-B", departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", line, 
          "Testination-C", departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", line, 
          "Testination-A", departureTime.plusMinutes(18), timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByDestination(
          false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that sortDeparturesByDestination correctly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByDestination Reverse Test") 
    void testSortDeparturesByDestinationRev() {

      // Arrange
      station.addDeparture("A", line, 
          "Testination-A", departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", line, 
          "Testination-C", departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("B", line, 
          "Testination-B", departureTime.plusMinutes(18), timeTolerance);
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByDestination(
          true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that getDeparturesByOriginalDepartureTime correctly
     * returns an empty list when there are no matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByOriginalDepartureTime Negative Test")
    void testGetDeparturesByOriginalDepartureTimeNeg() {

      // Arrange

      int timeIncrements = 6;
      int timeRangeSize = 2 * timeIncrements;
      LocalDateTime startRange = departureTime.minusMinutes(timeRangeSize);
      LocalDateTime endRange = departureTime.plusMinutes(timeRangeSize);

      // Act
      List<String> resultingList = station
          .getDeparturesByOriginalDepartureTime(startRange, endRange);

      // Assert
      assertTrue(resultingList.isEmpty()); 
    }

    /** Tests that getDeparturesByOriginalDepartureTime correctly
     * compiles a list of only matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByOriginalDepartureTime Possitive Test")
    void testGetDeparturesByOriginalDepartureTime() {

      // Arrange

      int timeIncrements = 6;
      station.addDeparture("A", line,
          destination, departureTime.minusMinutes(3 * timeIncrements), timeTolerance);
      station.addDeparture("B", line, 
          destination, departureTime.minusMinutes(2 * timeIncrements), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.minusMinutes(1 * timeIncrements), timeTolerance);
      station.addDeparture("D", line, 
          destination, departureTime.plusMinutes(1 * timeIncrements), timeTolerance);
      station.addDeparture("E", line, 
          destination, departureTime.plusMinutes(2 * timeIncrements), timeTolerance);
      station.addDeparture("F", line, 
          destination, departureTime.plusMinutes(3 * timeIncrements), timeTolerance);
      List<String> expectedList = Arrays.asList("B", "C", "D", "E");

      int timeRangeSize = 2 * timeIncrements;
      LocalDateTime startRange = departureTime.minusMinutes(timeRangeSize);
      LocalDateTime endRange = departureTime.plusMinutes(timeRangeSize);

      // Act
      List<String> resultingList = station
          .getDeparturesByOriginalDepartureTime(startRange, endRange);

      // Assert
      assertTrue(resultingList.containsAll(expectedList)); 
    }

    /** Tests that sortDeparturesByOriginalDepartureTime
     * throws the correct exception when
     * trying to sort departures that do not exist. 
     */
    @Test
    @DisplayName("sortDepartureByOriginalDepartureTime Negative Test")
    void testSortDepartureByOriginalDepartureTimeNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByOriginalDepartureTime(
            false, failTrainNumbers);
      });
    }

    /** Tests that sortDeparturesByOriginalDepartureTime correctly
     * sorts departures by original departure time.
     *
     */
    @Test
    @DisplayName("sortDeparturesByOriginalDepartureTime Test") 
    void testSortDeparturesByOriginalDepartureTime() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime, timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByOriginalDepartureTime(
          false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that sortDeparturesByOriginalDepartureTime correctly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByOriginalDepartureTime Reverse Test") 
    void testSortDeparturesByOriginalDepartureTimeRev() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime.plusMinutes(0), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByOriginalDepartureTime(
          true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that getDeparturesByCurrentDepartureTime correctly
     * returns an empty list when there are no matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByCurrentDepartureTime Negative Test")
    void testGetDeparturesByCurrentDepartureTimeNeg() {

      // Arrange

      int timeIncrements = 6;
      int timeRangeSize = 2 * timeIncrements;
      LocalDateTime startRange = departureTime.minusMinutes(timeRangeSize);
      LocalDateTime endRange = departureTime.plusMinutes(timeRangeSize);

      // Act
      List<String> resultingList = station
          .getDeparturesByCurrentDepartureTime(startRange, endRange);

      // Assert
      assertTrue(resultingList.isEmpty()); 
    }

    /** Tests that getDeparturesByCurrentDepartureTime correctly
     * compiles a list of only matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByCurrentDepartureTime Possitive Test")
    void testGetDeparturesByCurrentDepartureTime() {

      // Arrange

      int timeIncrements = 6;
      station.addDeparture("A", line,
          destination, departureTime.minusMinutes(3 * timeIncrements), timeTolerance);
      station.addDeparture("B", line, 
          destination, departureTime.minusMinutes(2 * timeIncrements), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.minusMinutes(1 * timeIncrements), timeTolerance);
      station.addDeparture("D", line, 
          destination, departureTime.plusMinutes(1 * timeIncrements), timeTolerance);
      station.addDeparture("E", line, 
          destination, departureTime.plusMinutes(2 * timeIncrements), timeTolerance);
      station.addDeparture("F", line, 
          destination, departureTime.plusMinutes(3 * timeIncrements), timeTolerance);
      List<String> expectedList = Arrays.asList("B", "C", "D", "E");

      int timeRangeSize = 2 * timeIncrements;
      LocalDateTime startRange = departureTime.minusMinutes(timeRangeSize);
      LocalDateTime endRange = departureTime.plusMinutes(timeRangeSize);

      // Act
      List<String> resultingList = station
          .getDeparturesByCurrentDepartureTime(startRange, endRange);

      // Assert
      assertTrue(resultingList.containsAll(expectedList)); 
    }

    /** Tests that sortDepartureByCurrentDepartureTime
     * throws the correct exception when trying to sort
     * departures that do not exist.
     *
     */
    @Test
    @DisplayName("sortDepartureByCurrentDepartureTime Negative Test")
    void testSortDepartureByCurrentDepartureTimeNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByCurrentDepartureTime(
            false, failTrainNumbers);
      });
    }

    /** Tests that sort DeparturesByCurrentDepartureTime correctly
     * sorts departures by current departure time.
     *
     */
    @Test
    @DisplayName("sortDeparturesByCurrentDepartureTime Test") 
    void testSortDeparturesByCurrentDepartureTime() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime, timeTolerance);
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByCurrentDepartureTime(
          false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that sortDeparturesByCurrentDepartureTime correcly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByCurrentDepartureTime Reverse Test") 
    void testSortDeparturesByCurrentDepartureTimeRev() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime.plusMinutes(0), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByCurrentDepartureTime(
          true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that getDeparturesByDelay correctly
     * returns an empty list when there are no matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByDelay Negative Test")
    void testGetDeparturesByDelayNeg() {

      // Arrange
      Duration delayMin = Duration.ofMinutes(-6);
      Duration delayMax = Duration.ofMinutes(6); 

      // Act
      List<String> resultingList = station
          .getDeparturesByDelay(delayMin, delayMax);

      // Assert
      assertTrue(resultingList.isEmpty()); 
    }

    /** Tests that getDeparturesByDuration correctly
     * compuiles a list of only matching Departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByDuration Possitive Test")
    void testGetDeparturesByDuration() {

      // Arrange

      station.addDeparture("A", "line-a",
          destination, departureTime, timeTolerance);
      station.addDeparture("B", "line-b", 
          destination, departureTime, timeTolerance);
      station.addDeparture("C", "line-c", 
          destination, departureTime, timeTolerance);
      station.addDeparture("D", "line-d", 
          destination, departureTime, timeTolerance);
      station.addDeparture("E", "line-e", 
          destination, departureTime, timeTolerance);
      station.addDeparture("F", "line-f", 
          destination, departureTime, timeTolerance);

      station.setDepartureDelay(Duration.ofMinutes(6), "B", timeTolerance);
      station.setDepartureDelay(Duration.ofMinutes(12), "C", timeTolerance);
      station.setDepartureDelay(Duration.ofMinutes(18), "D", timeTolerance);
      station.setDepartureDelay(Duration.ofMinutes(24), "E", timeTolerance);
      station.setDepartureDelay(Duration.ofMinutes(30), "F", timeTolerance);

      List<String> expectedList = Arrays.asList("B", "C", "D", "E");

      Duration delayMin = Duration.ofMinutes(6);
      Duration delayMax = Duration.ofMinutes(24); 

      // Act
      List<String> resultingList = station
          .getDeparturesByDelay(delayMin, delayMax);

      // Assert
      assertTrue(resultingList.containsAll(expectedList)); 
    }

    /** Tests that sortDepartureByDelay throws the correct
     * exception when trying to sort Departures that do not exist.
     *
     */
    @Test
    @DisplayName("sortDepartureByDelay Negative Test")
    void testSortDepartureByDelayNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByDelay(
            false, failTrainNumbers);
      });
    }

    /** Tests that sortDeparturesByDelay correctly
     * sorts by Delay.
     *
     */
    @Test
    @DisplayName("sortDeparturesByDelay Test") 
    void testSortDeparturesByDelay() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime, timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
          

      station.addDepartureDelay(Duration.ofMinutes(4), "C", timeTolerance);
      station.addDepartureDelay(Duration.ofMinutes(3), "B", timeTolerance);
      station.addDepartureDelay(Duration.ofMinutes(2), "A", timeTolerance);
      
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByDelay(
          false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that SortDeparturesByDelay correctly
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByDelay Reverse Test") 
    void testSortDeparturesByDelayRev() {

      // Arrange
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("A", line, 
          destination, departureTime, timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
          

      station.addDepartureDelay(Duration.ofMinutes(14), "C", timeTolerance);
      station.addDepartureDelay(Duration.ofMinutes(8), "B", timeTolerance);
      station.addDepartureDelay(Duration.ofMinutes(2), "A", timeTolerance);
      
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByDelay(
          true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that getDeparturesByTrack correctly
     * returns an empty list when there are no matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByTrack Negative Test")
    void testGetByDeparturesTrackNegative() {

      // Act
      List<String> noDepartures = station.getDeparturesByTrack(1);

      // Assert
      assertTrue(noDepartures.isEmpty());
    }

    /** Tests that getDeparturesByTrack correctly
     * compiles a list of only matching departures.
     *
     */
    @Test
    @DisplayName("getDeparturesByTrack Test") 
    void testGetDeparturesByTrackPopulated() {

      // Arrange
      station.addDeparture("A", line,
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("B", line, 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("C", line, 
          destination, departureTime.plusMinutes(18), timeTolerance);
      station.addDeparture("D", line, 
          destination, departureTime.plusMinutes(24), timeTolerance);
      station.addDeparture("E", line, 
          destination, departureTime.plusMinutes(30), timeTolerance);
      station.addDeparture("F", line, 
          destination, departureTime.plusMinutes(36), timeTolerance);
          
      station.setDepartureTrack("A", 2, timeTolerance);
      station.setDepartureTrack("B", 2, timeTolerance);
      station.setDepartureTrack("C", 2, timeTolerance);
      station.setDepartureTrack("D", 1, timeTolerance);
      station.setDepartureTrack("E", 1, timeTolerance);
      station.setDepartureTrack("F", 3, timeTolerance);
          
      List<String> expectedList = Arrays.asList("A", "B", "C");
      
      // Act
      List<String> departuresByTrack = station.getDeparturesByTrack(2);

      // Assert
      boolean haspassed = departuresByTrack.containsAll(expectedList)
          && !departuresByTrack.contains("D")
          && !departuresByTrack.contains("E")
          && !departuresByTrack.contains("F");
      assertTrue(haspassed);
    }

    /** Tests that sortDeparturesByTrack throws the correct exception
     * when trying to sort Departures that do not exist. 
     *
     */
    @Test
    @DisplayName("sortDeparturesByTrack Negative Test") 
    void testSortDeparturesByTrackNeg() {

      // Arrange
      List<String> failTrainNumbers = Arrays.asList("21", "11", "4");

      // Act-Assert
      assertThrows(NullPointerException.class, () -> {
        station.sortDeparturesByTrack(
            false, failTrainNumbers);
      });
    }

    /** Tests that sortDeparturesByTrack correctly
     * sorts by track.
     *
     */
    @Test
    @DisplayName("sortDeparturesByTrack Test") 
    void testSortDeparturesByTrack() {

      // Arrange
      station.addDeparture("B", "Line-B", 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", "Line-C", 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", "Line-A", 
          destination, departureTime.plusMinutes(18), timeTolerance);

      station.setDepartureTrack("A", 1, timeTolerance);
      station.setDepartureTrack("B", 2, timeTolerance);
      station.setDepartureTrack("C", 3, timeTolerance);


      
      List<String> expectedList = Arrays.asList("A", "B", "C");

      // Act
      List<String> sortedList = station.sortDeparturesByTrack(false, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that sortDeparturesByTrack correctly 
     * does reverse sort.
     *
     */
    @Test
    @DisplayName("sortDeparturesByTrack Reverse Test") 
    void testSortDeparturesByTrackRev() {

      // Arrange
      station.addDeparture("B", "Line-B", 
          destination, departureTime.plusMinutes(6), timeTolerance);
      station.addDeparture("C", "Line-C", 
          destination, departureTime.plusMinutes(12), timeTolerance);
      station.addDeparture("A", "Line-A", 
          destination, departureTime.plusMinutes(18), timeTolerance);

      station.setDepartureTrack("A", 1, timeTolerance);
      station.setDepartureTrack("B", 2, timeTolerance);
      station.setDepartureTrack("C", 3, timeTolerance);
      
      List<String> expectedList = Arrays.asList("C", "B", "A");

      // Act
      List<String> sortedList = station.sortDeparturesByTrack(true, station.getAllDepartures());

      // Assert
      assertEquals(expectedList, sortedList);
    }

    /** Tests that setDepartureTrack correctly handles 
     * setting valid and invalid track numbers.
     *
     * @param myTrackNumber An int representing a track number.
     * @param possitive A boolean, devides if a possitive or negative test is run for the testdata.
     */
    @ParameterizedTest
    @DisplayName("setDepartureTrack test")
    @CsvSource({
      "-2, false",
      "-1, true",
      "0, false",
      "1, true",
      "50, true",
      "1000, true",
    })
    void testDepartureSetTrack(int myTrackNumber, boolean possitive) {
      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      
      // Act
      boolean result = station.setDepartureTrack(trainNumber, myTrackNumber, timeTolerance);

      // Assert
      if (possitive) {
        assertTrue(result);
      } else {
        assertFalse(result);;
      }
    }

    /** Tests that getDepartureTrack gets the correct track.
     *
     * @param myTrackNumber
     */
    @ParameterizedTest
    @CsvSource({
      "-1",
      "1",
      "50",
      "1000",
    })
    @DisplayName("getDepartureTrack test")
    void testGetDepartureTrack(int myTrackNumber) {
      // Arrange
      int result;
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      station.setDepartureTrack(trainNumber, myTrackNumber, timeTolerance);

      // Act
      result = station.getDepartureTrack(trainNumber);
      // Assert
      assertEquals(result, myTrackNumber);
    }

    /** Tests that getDepartureOriginalDepartureTime gets the correct
     * LocalDateTime.
     * 
     */
    @Test
    @DisplayName("getDepartureOriginalDepartureTime Test")
    void testGetDepartureOriginalDepartureTime() {

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);

      // Act
      LocalDateTime result = station.getDepartureOriginalDepartureTime(trainNumber);

      // Assert
      assertEquals(result, departureTime);
    }

    /** Tests that getDepartureCurrentDepartureTime gets the correct 
     * LocalDateTime.
     *
     * @param delay A Duration representing the additional deviation from original departure time.
     */
    @ParameterizedTest
    @CsvSource({
      "0, true",
      "1, true",
      "10, true",
      "100, true",
      "1000, true",
    })
    @DisplayName("getDepartureCurrentDepartureTime Test")
    void testgetDepartureCurrentDepartureTime(int delay) {

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      station.addDepartureDelay(Duration.ofMinutes(delay), trainNumber, timeTolerance);
      LocalDateTime withDelay = departureTime.plus(Duration.ofMinutes(delay));

      // Act
      LocalDateTime result = station.getDepartureCurrentDepartureTime(trainNumber);

      // Assert
      assertEquals(result, withDelay);
    }

    /** Tests that SetDepartureDelay correctly handles for
     *  setting delays.
     *
     * @param delay A Duration representing the additional deviation from original departure time.
     * @param possitive A boolean, devides if a possitive or negative test is run for the testdata.
     */
    @ParameterizedTest
    @CsvSource({
      "-10, false",
      "-1, false",
      "0, true",
      "1, true",
      "10, true",
      "100, true",
      "1000, true",
    })
    @DisplayName("setDepartureDelay Test")
    void testsetDepartureDelay(int delay, boolean possitive) {

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      Duration myDelay = Duration.ofMinutes(delay);
      
      // Act
      boolean result = station.setDepartureDelay(myDelay, trainNumber, timeTolerance);

      // Assert
      if (possitive) {
        assertTrue(result);
      } else {
        assertFalse(result);
      }
    }


    /** Tests that SetDepartureDelay correctly handles for
     *  departure conflicts between Departures with the same line.
     *
     * @param delay A Duration representing the additional deviation from original departure time.
     * @param possitive A boolean, devides if a possitive or negative test is run for the testdata.
     */
    @ParameterizedTest
    @CsvSource({
      "15, false",
      "16, true",
    })
    @DisplayName("setDepartureDelay Line Conflict Test")
    void testsetDepartureDelayLineConflict(int delay, boolean possitive) {

      // Arrange
      boolean result; 
      Duration myDelay = Duration.ofMinutes(delay);
      station.addDeparture("A", line, destination, 
          departureTime.minus(timeTolerance), timeTolerance);
      station.addDeparture("B", line, destination, 
          departureTime.plus(timeTolerance), timeTolerance);
      
      // Act
      try {
        station.setDepartureDelay(myDelay, "A", timeTolerance);
        result = true;
      } catch (IllegalArgumentException e) {
        result = false;
      }

      // Assert
      if (possitive) {
        assertTrue(result);
      } else {
        assertFalse(result);
      }
    }


    /** Tests that SetDepartureDelay correctly handles for
     *  departure conflicts between Departures with the same track.
     *
     * @param delay A Duration representing the additional deviation from original departure time.
     * @param possitive A boolean, devides if a possitive or negative test is run for the testdata.
     */
    @ParameterizedTest
    @CsvSource({
      "15, false",
      "16, true",
    })
    @DisplayName("setDepartureDelay Track Conflict Test")
    void testsetDepartureDelayTrackConflict(int delay, boolean possitive) {

      // Arrange
      boolean result;
      Duration myDelay = Duration.ofMinutes(delay);
      
      station.addDeparture("A", "Line-A", destination, 
          departureTime.minus(timeTolerance), timeTolerance);
      
      station.setDepartureTrack("A", 1, timeTolerance);
      
      station.addDeparture("B", "Line-B", destination, 
          departureTime.plus(timeTolerance), timeTolerance);
      
      station.setDepartureTrack("B", 1, timeTolerance);
      
      // Act
      try {
        station.setDepartureDelay(myDelay, "A", timeTolerance);
        result = true;
      } catch (IllegalArgumentException e) {
        result = false;
      }

      // Assert
      if (possitive) {
        assertTrue(result);
      } else {
        assertFalse(result);
      }
    }

    /** Tests that the getDepartureDelay method gets the correct values.
     *
     * @param delay A Duration representing the additional deviation from original departure time.
     */
    @ParameterizedTest
    @CsvSource({
      "0, true",
      "1, true",
      "10, true",
      "100, true",
      "1000, true",
    })
    @DisplayName("getDepartureDelay Test")
    void testgetDepartureDelay(int delay) {

      // Arrange
      station.addDeparture(trainNumber, line, destination, departureTime, timeTolerance);
      Duration myDelay = Duration.ofMinutes(delay);
      station.setDepartureDelay(myDelay, trainNumber, timeTolerance);

      // Act
      Duration result = station.getDepartureDelay(trainNumber);

      // Assert
      assertEquals(result, myDelay);
    }
  }
}
